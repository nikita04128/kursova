<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Редагування</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
    <style>
    #back {
  background-image: url(https://www.fg-a.com/wallpapers/white-background-retro-shapes.jpg);
  background-repeat: no-repeat;
   background-size: 1920px 1080px;
}
</style>
</head>
<body id="back">
<div class="container mt-2">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Редагування</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('posts.index') }}" enctype="multipart/form-data">Назад</a>
            </div>
        </div>
    </div>

  @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
    </div>
  @endif

    <form action="{{ route('posts.update',$post->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>ФІО студента</strong>
                    <input type="text" name="title" value="{{ $post->title }}" class="form-control" placeholder="ФІО">
                    @error('title')
                     <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Група</strong>
                        <input type="text" name="groupa" value="{{ $post->groupa }}" class="form-control" placeholder="ЛА-">
                        @error('title')
                         <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Стипендія</strong>
                    <input type="text" name="rating" value="{{ $post->rating }}" class="form-control" placeholder="Кількість">
                    @error('title')
                     <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Стипендія</strong>
                    <input type="text" name="scholar" value="{{ $post->scholar }}" class="form-control" placeholder="Кількість">
                    @error('title')
                     <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Гуртожиток</strong>
                    <input type="text" name="hostel" value="{{ $post->hostel }}" class="form-control" placeholder="Номер">
                    @error('title')
                     <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Фото</strong>
                 <input type="file" name="image" class="form-control" placeholder="Post Title">
                @error('image')
                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
            </div>
            <div class="form-group">
                <img src="{{ Storage::url($post->image) }}" height="200" width="200" alt="" />
              </div>
          </div>
                <button type="submit" class="btn btn-primary ml-3">Відправити</button>

          </div>

      </form>
  </div>
</body>
</html>
