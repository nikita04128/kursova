<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Деканат</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
    <style>
    #back {
  background-image: url(https://www.fg-a.com/wallpapers/white-background-retro-shapes.jpg);
  background-repeat: no-repeat;
   background-size: 1920px 1080px;
}

.zoom {
  transition: transform .2s;
}

.zoom:hover {
  transform: scale(1.5);
}
    </style>
</head>
<body id="back">
<div  class="container mt-2">
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Система Деканат</h2>
                <h3 style="font-size: 12px;">(Дісклеймер: всі збіги фото та фіо з реальними людьми - випадкові)</h3>
            </div>
            <div class="pull-right mb-1">
            </div>
        </div>
        <form style=" position: relative; left: 15px;"action="/search" method="get">
          <div class="input-group">
            <input type="search" name="search" class="form-control"
            <span  class="input-group-prepend">
              <button type="submit" class="btn btn-primary">Пошук</button>
            </span>
          </div>
        </form>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th width="20px">@sortablelink('id', 'id')</th>
            <th width="90px">Фото</th>
            <th>             @sortablelink('title', 'ФІО')</th>
            <th width="20px">@sortablelink('groupa', 'Група')</th>
            <th width="20px">@sortablelink('rating', 'Рейтинг')</th>
            <th width="20px">@sortablelink('hostel', 'Гуртожиток')</th>
            <th width="100px">@sortablelink('scholar', 'Стипендія')</th>
        </tr>
        @if($posts->count())
        @foreach ($posts as $post)
        @php
        $newh = preg_replace('/^0/', 'Немає', $post->hostel, 1);
        $news = preg_replace('/^0/', 'Немає', $post->scholar, 1);
        @endphp


        <tr>
            <td>{{ $post->id }}</td>
            <td><img class="zoom" src="{{ Storage::url($post->image) }}" height="80 " width="80" alt="" /></td>
            <td>{{ $post->title  }}</td>
            <td>{{ $post->groupa }}</td>
            <td>{{ $post->rating }}</td>
            <td>{{ $newh  }}</td>
            <td>{{ $news }}</td>
        </tr>
        @endforeach
        @endif
    </table>

          {!! $posts->appends(\Request::except('page'))->render() !!}

</body>
</html>
