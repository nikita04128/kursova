<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Post extends Model
{
    use HasFactory, Sortable;
  //  protected $fillable = [
  //      'title', 'groupa', 'scholar', 'hostel'];
    	public $sortable = ['id', 'title', 'groupa', 'rating', 'scholar', 'hostel'];

}
