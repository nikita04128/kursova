<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MainCRUDController extends Controller
{
    public function index()
    {
       $data['posts'] = Post::sortable()->paginate(5);
      return view('dashboard', $data);
    }

    public function search(Request $request)
    {
      $search = $request->get('search');
      $posts = \DB::table('posts')->where('title', 'like', '%'.$search.'%')->paginate(5);
      return view('posts/index', ['posts' => $posts]);
    }
    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }


}
