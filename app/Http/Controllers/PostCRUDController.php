<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostCRUDController extends Controller
{


    public function index()
    {
       $data['posts'] = Post::sortable()->paginate(5);
      return view('posts.index', $data);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function search(Request $request)
    {
      $search = $request->get('search');
      $posts = \DB::table('posts')->where('title', 'like', '%'.$search.'%')->paginate(5);
      return view('posts/index', ['posts' => $posts]);
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        $path = $request->file('image')->store('public/images');
        $post = new Post;
        $post->title = $request->title;
        $post->image = $path;
        $post->groupa = $request->groupa;
        $post->rating = $request->rating;
        $post->hostel = $request->hostel;
        $post->scholar = $request->scholar;
        $post->save();

        return redirect()->route('posts.index')
                        ->with('success','Студент пройшов операцію');
    }

    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }


    public function edit(Post $post)
    {
        return view('posts.edit',compact('post'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $post = Post::find($id);
        if($request->hasFile('image')){
            $request->validate([
              'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $path = $request->file('image')->store('public/images');
            $post->image = $path;
        }
        $post->title = $request->title;
        $post->groupa = $request->groupa;
        $post->hostel = $request->hostel;
        $post->rating = $request->rating;
        $post->scholar = $request->scholar;
        $post->save();

        return redirect()->route('posts.index')
                        ->with('success','Ого, воно працює: Операція успішна');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index')
                        ->with('success','Военком буде рад новобранцям');
    }
}
