<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
  public function doSomething()
  {
      //что-то делает
  }
  public function testStub()
  {
      $stub = $this->getMockBuilder('SomeClass')
                      ->setMethods(array('doSomething'))
                      ->getMock();

      $stub->expects($this->any())
              ->method('doSomething')
              ->will($this->returnValue('lol'));

      $this->assertEquals('lol', $stub->doSomething());
  }

  
  public function testBasicTest()
  {
      $this->assertTrue(true);
  }

  }
