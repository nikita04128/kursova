<?php

class FirstCest
{
    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Система Деканат');
    }/**     * @dataProvider pageProvider
     */
     protected function pageProvider()
         {
             return [
                 ['page'=>"?page=2", 'id'=>"108"],
             ];
         }

    public function login(AcceptanceTester $I)
    {
        $I->amOnPage('/auth/login');
        $I->see('Логін');
        $I->fillField('email',"admin@admin.com");
        $I->fillField('password',"12345");
        $I->click('Sign In');
        $I->canSee('Система Деканат');

    }
}
