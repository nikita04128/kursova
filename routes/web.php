<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostCRUDController;
use App\Http\Controllers\MainCRUDController;
use App\Http\Controllers\UserAuthController;

Route::resource('/', MainCRUDController::class);


Route::post('/auth/save',[UserAuthController::class, 'save'])->name('auth.save');
Route::post('/auth/check',[UserAuthController::class, 'check'])->name('auth.check');
Route::get('/auth/logout',[UserAuthController::class, 'logout'])->name('auth.logout');




Route::group(['middleware'=>['AuthCheck']], function(){
    Route::get('/auth/login',[UserAuthController::class, 'login'])->name('auth.login');
    Route::get('/auth/register',[UserAuthController::class, 'register'])->name('auth.register');
    Route::resource('posts', PostCRUDController::class);

});

Route::get('/search','App\Http\Controllers\PostCRUDController@search');
